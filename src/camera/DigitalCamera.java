
package camera;


public abstract class DigitalCamera {
 protected String Make;
 protected String Model;
 protected String Megapixels;
 protected double InternalMemorySize;
 protected double ExternalMemorySize;
DigitalCamera(){
    
}


 DigitalCamera(String Make, String Model, String MegaPixels, double InternalMemorySize,double ExternalMemorySize ){
     this.Make = Make;
     this.Model = Model;
     this.Megapixels = MegaPixels;
     this.InternalMemorySize = InternalMemorySize;
     this.ExternalMemorySize = ExternalMemorySize;
 }
    public abstract String getMake();

    

    public abstract String getModel() ;

  
    

    public abstract String getMegapixels() ;
  
    public abstract double getInternalMemorySize() ;

 
    public abstract double getExternalMemorySize();
    
    public abstract String describeCamera();


 
 
}

   