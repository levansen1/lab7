
package camera;

public class PointAndShootCamera extends DigitalCamera{
   PointAndShootCamera(){
       
   }
   PointAndShootCamera(String Make, String Model, String Megapixels,  double ExternalMemorySize){
      this.Make = Make;
     this.Model = Model;
     this.Megapixels = Megapixels;
     this.ExternalMemorySize = ExternalMemorySize;
      
   }
       public String getMake() {
        return Make;
    }

   

    public String getModel() {
        return Model;
    }

   

    public String getMegapixels() {
        return Megapixels;
    }

    public double getInternalMemorySize() {
        return InternalMemorySize;
    }

   

    public double getExternalMemorySize() {
        return ExternalMemorySize;
    }

   public String describeCamera(){
       return String.format("Point and shoot camera details: \n Make: %s, Model: %s, pixels: %s, Internal memory size: %f, External memory size: %f",
               getMake(), getModel(), getMegapixels(),getInternalMemorySize(),getExternalMemorySize() );
   }

}
