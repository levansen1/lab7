
package camera;


public class PhoneCamera extends DigitalCamera{
    
    PhoneCamera(){
        
    }
    
    PhoneCamera(String Make, String Model, String Megapixels, double InternalMemorySize){
      this.Make = Make;
     this.Model = Model;
     this.Megapixels = Megapixels;
     this.InternalMemorySize = InternalMemorySize;
      
   }
       public String getMake() {
        return Make;
    }

   

    public String getModel() {
        return Model;
    }

   

    public String getMegapixels() {
        return Megapixels;
    }

    public double getInternalMemorySize() {
        return InternalMemorySize;
    }

   

    public double getExternalMemorySize() {
        return ExternalMemorySize;
    }

   
   public String describeCamera(){
       return String.format("Phone camera details: \n Make: %s, Model: %s, pixels: %s, Internal memory size: %f, External memory size: %f",
               getMake(), getModel(), getMegapixels(),getInternalMemorySize(),getExternalMemorySize() );
   }

}


